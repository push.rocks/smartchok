import { tap, expect } from '@pushrocks/tapbundle';
import * as smartchok from '../ts/index';
import * as smartfile from '@pushrocks/smartfile';
import * as smartpromise from '@pushrocks/smartpromise';
import * as smartrx from '@pushrocks/smartrx';

import * as fs from 'fs';

// the module to test
if (process.env.CI) {
  process.exit(0);
}

let testSmartchok: smartchok.Smartchok;
let testAddObservable: smartrx.rxjs.Observable<[string, fs.Stats]>;
let testSubscription: smartrx.rxjs.Subscription;
tap.test('should create a new instance', async () => {
  testSmartchok = new smartchok.Smartchok([]);
  expect(testSmartchok).to.be.instanceof(smartchok.Smartchok);
});

tap.test('should add some files to watch and start', async () => {
  testSmartchok.add(['./test/assets/**/*.txt']);
  await testSmartchok.start()
  testSmartchok.add(['./test/assets/**/*.md']);
});

tap.test('should get an observable for a certain event', async () => {
  await testSmartchok.getObservableFor('add').then(async (observableArg) => {
    testAddObservable = observableArg;
  });
});

tap.test('should register an add operation', async () => {
  let testDeferred = smartpromise.defer();
  testSubscription = testAddObservable.subscribe(pathArg => {
    const pathResult = pathArg[0];
    console.log(pathResult);
    testDeferred.resolve();
  });
  smartfile.memory.toFs('HI', './test/assets/hi.txt');
  await testDeferred.promise;
});

tap.test('should stop the watch process', async () => {
  testSmartchok.stop();
});

tap.start();
