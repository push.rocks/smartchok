// node native
import * as fs from 'fs';
import * as path from 'path';

export {
  fs,
  path
}

// @pushrocks scope
import * as lik from '@pushrocks/lik';
import * as smartpromise from '@pushrocks/smartpromise';
import * as smartrx from '@pushrocks/smartrx';

export {
  lik,
  smartpromise,
  smartrx
}

// thirdparty scope
import * as chokidar from 'chokidar';

export {
  chokidar
}